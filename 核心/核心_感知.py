#模块 核心_感知.py
#作者 幽灵代码
#QQ 29672366
#邮箱 29672366@qq.com
#日期 2016.3.21

__核心_感知_版本="核心_感知_1_0"
__核心_感知_描述="感知模型"

__核心_感知_分类映射表 = {}
__核心_感知_对象映射表 = {}
__核心_感知_属性映射表 = {}
__核心_感知_执行映射表 = {}

__核心_感知_执行映射表["终端输入"] = "核心_感知_终端输入"
__核心_感知_执行映射表["终端输出"] = "核心_感知_终端输出"
__核心_感知_执行映射表["任务队列"] = "核心_感知_任务队列"
__核心_感知_执行映射表["任务处理"] = "核心_感知_任务处理"

__核心_感知_回调  = {}

#版本
def 核心_感知_版本():
    return __核心_感知_版本

#描述
def 核心_感知_描述():
    return __核心_感知_描述

#分类映射表
def 核心_感知_分类映射表():
    return __核心_感知_分类映射表

#对象映射表
def 核心_感知_对象映射表():
    return __核心_感知_对象映射表

#属性映射表
def 核心_感知_属性映射表():
    return __核心_感知_属性映射表

#执行映射表
def 核心_感知_执行映射表():
    return __核心_感知_执行映射表

#初始化
def 核心_感知_初始化(词汇解释,语句解释):
    __核心_感知_回调["词汇解释"] = 词汇解释
    __核心_感知_回调["语句解释"] = 语句解释

#执行
def 核心_感知_分发器(词汇,d,g):
    if 词汇 in __核心_感知_分类映射表:
        return eval(__核心_感知_分类映射表[词汇])(d,g)
    if 词汇 in __核心_感知_对象映射表:
        return eval(__核心_感知_对象映射表[词汇])(d,g)
    if 词汇 in __核心_感知_属性映射表:
        return eval(__核心_感知_属性映射表[词汇])(d,g)
    if 词汇 in __核心_感知_执行映射表:
        return eval(__核心_感知_执行映射表[词汇])(d,g)
#结果集
lstres = []

def 核心_感知_终端输入(d,g):
    #print(d)
    global lstres
    sentense = d["待处理语句"]
    if(sentense==""):
        核心_感知_任务处理(d,g)
        return
    lstres.extend(__核心_感知_回调["语句解释"](d))
    if(len(lstres)):
        核心_感知_终端输入(lstres.pop(),g)
    else:
        d["待处理语句"] = sentense[1:]
        if "未知" in d:
            d["未知"] = d["未知"]+sentense[:1]
        else:
            d["未知"] = sentense[:1]
        核心_感知_终端输入(d,g)

#属性集
lstAttr = []

def 核心_感知_任务队列(d,g):
    global lstAttr
    if "告知属性" in d:
        lstAttr.append(d["告知属性"])
        
def 核心_感知_任务处理(d,g):
    result = ""
    global lstAttr
    for info in lstAttr:
        result = result+d.get(info,'') + ","
    if "未处理属性" in d:
        tempstr = d["未处理属性"]
        if d["未处理属性"] in d:
            result = result+d[d["未处理属性"]] + ","
    if(result==""):
        if(len(lstres)>0):
            核心_感知_终端输入(lstres.pop(),g)
            return
        else:
            result = "不知道."
    result = result[:-1]
    if "消息" in d:
        result = d["消息"]
    r = {"终端输出":result}
    核心_感知_终端输出(r,g)
    if(len(lstres)==0):
        lstAttr=[]

def 核心_感知_终端输出(d,g):
    if "终端输出" in d:
        print(d["终端输出"])
