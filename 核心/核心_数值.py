#模块 核心_数值.py
#作者 幽灵代码
#QQ 29672366
#邮箱 29672366@qq.com
#日期 2016.3.23

__核心_数值_版本="核心_数值_1_0"
__核心_数值_描述="为机器智能赋予数值概念"

__核心_数值_分类映射表 = {}
__核心_数值_对象映射表 = {}
__核心_数值_属性映射表 = {}
__核心_数值_执行映射表 = {}

__核心_数值_执行映射表["."] = "核心_数值_点"
__核心_数值_执行映射表["1"] = "核心_数值_1"
__核心_数值_执行映射表["2"] = "核心_数值_2"
__核心_数值_执行映射表["3"] = "核心_数值_3"
__核心_数值_执行映射表["4"] = "核心_数值_4"
__核心_数值_执行映射表["5"] = "核心_数值_5"
__核心_数值_执行映射表["6"] = "核心_数值_6"
__核心_数值_执行映射表["7"] = "核心_数值_7"
__核心_数值_执行映射表["8"] = "核心_数值_8"
__核心_数值_执行映射表["9"] = "核心_数值_9"
__核心_数值_执行映射表["0"] = "核心_数值_0"
__核心_数值_执行映射表["点"] = "核心_数值_点"
__核心_数值_执行映射表["零"] = "核心_数值_0"
__核心_数值_执行映射表["一"] = "核心_数值_1"
__核心_数值_执行映射表["二"] = "核心_数值_2"
__核心_数值_执行映射表["三"] = "核心_数值_3"
__核心_数值_执行映射表["四"] = "核心_数值_4"
__核心_数值_执行映射表["五"] = "核心_数值_5"
__核心_数值_执行映射表["六"] = "核心_数值_6"
__核心_数值_执行映射表["七"] = "核心_数值_7"
__核心_数值_执行映射表["八"] = "核心_数值_8"
__核心_数值_执行映射表["九"] = "核心_数值_9"
__核心_数值_执行映射表["十"] = "核心_数值_十"
__核心_数值_执行映射表["百"] = "核心_数值_百"
__核心_数值_执行映射表["千"] = "核心_数值_千"
__核心_数值_执行映射表["万"] = "核心_数值_万"
__核心_数值_执行映射表["亿"] = "核心_数值_亿"
__核心_数值_执行映射表["两"] = "核心_数值_2"


__核心_数值_回调  = {}

#版本
def 核心_数值_版本():
    return __核心_数值_版本

#描述
def 核心_数值_描述():
    return __核心_数值_描述

#分类映射表
def 核心_数值_分类映射表():
    return __核心_数值_分类映射表

#对象映射表
def 核心_数值_对象映射表():
    return __核心_数值_对象映射表

#属性映射表
def 核心_数值_属性映射表():
    return __核心_数值_属性映射表

#执行映射表
def 核心_数值_执行映射表():
    return __核心_数值_执行映射表


#初始化
def 核心_数值_初始化(词汇解释,语句解释):
    __核心_数值_回调["词汇解释"] = 词汇解释
    __核心_数值_回调["语句解释"] = 语句解释

#执行
def 核心_数值_分发器(词汇,d,g):
    if 词汇 in __核心_数值_分类映射表:
        return eval(__核心_数值_分类映射表[词汇])(d,g)
    if 词汇 in __核心_数值_对象映射表:
        return eval(__核心_数值_对象映射表[词汇])(d,g)
    if 词汇 in __核心_数值_属性映射表:
        return eval(__核心_数值_属性映射表[词汇])(d,g)
    if 词汇 in __核心_数值_执行映射表:
        return eval(__核心_数值_执行映射表[词汇])(d,g)

#十
def 核心_数值_十(d,g):
    resdict = {}
    if "浮点数" in d:
        resdict["浮点数"] = "%f"%(float(d["浮点数"])*10)
        resdict["浮点数"] = resdict["浮点数"].rstrip('0')
        resdict["浮点数"] = resdict["浮点数"].rstrip('.')
    else:
        resdict["浮点数"] = "10"
    return resdict

#百
def 核心_数值_百(d,g):
    resdict = {}
    if "浮点数" in d:
        resdict["浮点数"] = "%f"%(float(d["浮点数"])*100)
        resdict["浮点数"] = resdict["浮点数"].rstrip('0')
        resdict["浮点数"] = resdict["浮点数"].rstrip('.')
    else:
        resdict["浮点数"] = "100"
    return resdict

#千
def 核心_数值_千(d,g):
    resdict = {}
    if "浮点数" in d:
        resdict["浮点数"] = "%f"%(float(d["浮点数"])*1000)
        resdict["浮点数"] = resdict["浮点数"].rstrip('0')
        resdict["浮点数"] = resdict["浮点数"].rstrip('.')
    else:
        resdict["浮点数"] = "1000"
    return resdict

#万
def 核心_数值_万(d,g):
    resdict = {}
    if "浮点数" in d:
        resdict["浮点数"] = "%f"%(float(d["浮点数"])*10000)
        resdict["浮点数"] = resdict["浮点数"].rstrip('0')
        resdict["浮点数"] = resdict["浮点数"].rstrip('.')
    else:
        resdict["浮点数"] = "10000"
    return resdict

#亿
def 核心_数值_亿(d,g):
    resdict = {}
    if "浮点数" in d:
        resdict["浮点数"] = "%f"%(float(d["浮点数"])*100000000)
        resdict["浮点数"] = resdict["浮点数"].rstrip('0')
        resdict["浮点数"] = resdict["浮点数"].rstrip('.')
    else:
        resdict["浮点数"] = "100000000"
    return resdict

#1
def 核心_数值_1(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "1"
        else:
            resdict["浮点数"] = d["浮点数"]+"1"
    else:
        resdict["浮点数"] = "1"
    return resdict

#2
def 核心_数值_2(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "2"
        else:
            resdict["浮点数"] = d["浮点数"]+"2"
    else:
        resdict["浮点数"] = "2"
    return resdict

#3
def 核心_数值_3(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "3"
        else:
            resdict["浮点数"] = d["浮点数"]+"3"
    else:
        resdict["浮点数"] = "3"
    return resdict

#4
def 核心_数值_4(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "4"
        else:
            resdict["浮点数"] = d["浮点数"]+"4"
    else:
        resdict["浮点数"] = "4"
    return resdict

#5
def 核心_数值_5(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "5"
        else:
            resdict["浮点数"] = d["浮点数"]+"5"
    else:
        resdict["浮点数"] = "5"
    return resdict

#6
def 核心_数值_6(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "6"
        else:
            resdict["浮点数"] = d["浮点数"]+"6"
    else:
        resdict["浮点数"] = "6"
    return resdict

#7
def 核心_数值_7(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "7"
        else:
            resdict["浮点数"] = d["浮点数"]+"7"
    else:
        resdict["浮点数"] = "7"
    return resdict

#8
def 核心_数值_8(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "8"
        else:
            resdict["浮点数"] = d["浮点数"]+"8"
    else:
        resdict["浮点数"] = "8"
    return resdict

#9
def 核心_数值_9(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "9"
        else:
            resdict["浮点数"] = d["浮点数"]+"9"
    else:
        resdict["浮点数"] = "9"
    return resdict

#0
def 核心_数值_0(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0"):
            resdict["浮点数"] = "0"
        else:
            resdict["浮点数"] = d["浮点数"]+"0"
    else:
        resdict["浮点数"] = "0"
    return resdict

#.
def 核心_数值_点(d,g):
    resdict = {}
    if "浮点数" in d:
        if(d["浮点数"] == "0" or d["浮点数"] == "0."):
            d["浮点数"] = "0."
        elif "." in d["浮点数"]:
            resdict["浮点数"] = d["浮点数"]
        else:
            resdict["浮点数"] = d["浮点数"]+"."
    else:
        resdict["浮点数"] = "0."
    return resdict
