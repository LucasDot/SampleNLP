#模块 核心_时间.py
#作者 幽灵代码
#QQ 29672366
#邮箱 29672366@qq.com
#日期 2016.3.21

import time
import datetime

__核心_时间_版本="核心_时间_1_0"
__核心_时间_描述="为机器智能赋予时间概念"

__核心_时间_分类映射表 = {}
__核心_时间_对象映射表 = {}
__核心_时间_属性映射表 = {}
__核心_时间_执行映射表 = {}

__核心_时间_执行映射表['今年'] = "核心_时间_今年"
__核心_时间_执行映射表['去年'] = "核心_时间_去年"
__核心_时间_执行映射表['明年'] = "核心_时间_明年"
__核心_时间_执行映射表['今天'] = "核心_时间_今天"
__核心_时间_执行映射表['昨天'] = "核心_时间_昨天"
__核心_时间_执行映射表['前天'] = "核心_时间_前天"
__核心_时间_执行映射表['明天'] = "核心_时间_明天"
__核心_时间_执行映射表['后天'] = "核心_时间_后天"
__核心_时间_执行映射表['现在'] = "核心_时间_现在"
__核心_时间_属性映射表['年'] = "核心_时间_年"
__核心_时间_属性映射表['月'] = "核心_时间_月"
__核心_时间_属性映射表['日'] = "核心_时间_日"
__核心_时间_属性映射表['号'] = "核心_时间_号"
__核心_时间_属性映射表['时'] = "核心_时间_时"
__核心_时间_属性映射表['分'] = "核心_时间_分"
__核心_时间_属性映射表['秒'] = "核心_时间_秒"
__核心_时间_属性映射表['时候'] = "核心_时间_时候"
__核心_时间_属性映射表['点'] = "核心_时间_点"
__核心_时间_属性映射表['点钟'] = "核心_时间_点"
__核心_时间_属性映射表['时区'] = "核心_时间_时区"
__核心_时间_属性映射表['日期'] = "核心_时间_日期"
__核心_时间_属性映射表['时间'] = "核心_时间_时间"
__核心_时间_属性映射表['星期'] = "核心_时间_星期"

__核心_时间_回调  = {}

#版本
def 核心_时间_版本():
    return __核心_时间_版本

#描述
def 核心_时间_描述():
    return __核心_时间_描述

#分类映射表
def 核心_时间_分类映射表():
    return __核心_时间_分类映射表

#对象映射表
def 核心_时间_对象映射表():
    return __核心_时间_对象映射表

#属性映射表
def 核心_时间_属性映射表():
    return __核心_时间_属性映射表

#执行映射表
def 核心_时间_执行映射表():
    return __核心_时间_执行映射表

#初始化
def 核心_时间_初始化(词汇解释,语句解释):
    __核心_时间_回调 ["词汇解释"] = 词汇解释
    __核心_时间_回调["语句解释"] = 语句解释

#分发器
def 核心_时间_分发器(词汇,d,g):
    if 词汇 in __核心_时间_分类映射表:
        return eval(__核心_时间_分类映射表[词汇])(d,g)
    if 词汇 in __核心_时间_对象映射表:
        return eval(__核心_时间_对象映射表[词汇])(d,g)
    if 词汇 in __核心_时间_属性映射表:
        return eval(__核心_时间_属性映射表[词汇])(d,g)
    if 词汇 in __核心_时间_执行映射表:
        return eval(__核心_时间_执行映射表[词汇])(d,g)

#星期字典
weekdic = {'Sun':'星期日',
        'Mon':'星期一',
        'Tue':'星期二',
        'Wed':'星期三',
        'Thu':'星期四',
        'Fri':'星期五',
        'Sat':'星期六'}

def 核心_时间_今年(d,g):
    resdict = {}
    timeStamp = int(time.time())
    timeArray = time.localtime(timeStamp)
    year = int(time.strftime("%Y", timeArray))
    resdict['年'] = "%d"%year
    return resdict

def 核心_时间_去年(d,g):
    resdict = {}
    timeStamp = int(time.time())
    timeArray = time.localtime(timeStamp)
    year = int(time.strftime("%Y", timeArray))
    year = year - 1
    resdict['年'] = "%d"%year
    return resdict

def 核心_时间_明年(d,g):
    resdict = {}
    timeStamp = int(time.time())
    timeArray = time.localtime(timeStamp)
    year = int(time.strftime("%Y", timeArray))
    year = year + 1
    resdict['年'] = "%d"%year
    return resdict

def 核心_时间_今天(d,g):
    resdict = {}
    today=datetime.date.today()
    timeArray = time.strptime(today.isoformat(), "%Y-%m-%d")
    resdict['年'] = time.strftime("%Y", timeArray)
    resdict['月'] = time.strftime("%m", timeArray)
    resdict['日'] = time.strftime("%d", timeArray)
    resdict['号'] = time.strftime("%d", timeArray)
    return resdict

def 核心_时间_昨天(d,g):
    resdict = {}
    today=datetime.date.today()
    delday=datetime.timedelta(days=1)
    yesterday=today-delday
    timeArray = time.strptime(yesterday.isoformat(), "%Y-%m-%d")
    resdict['年'] = time.strftime("%Y", timeArray)
    resdict['月'] = time.strftime("%m", timeArray)
    resdict['日'] = time.strftime("%d", timeArray)
    resdict['号'] = time.strftime("%d", timeArray)
    return resdict

def 核心_时间_前天(d,g):
    resdict = {}
    today=datetime.date.today()
    delday=datetime.timedelta(days=2)
    yesterday=today-delday
    timeArray = time.strptime(yesterday.isoformat(), "%Y-%m-%d")
    resdict['年'] = time.strftime("%Y", timeArray)
    resdict['月'] = time.strftime("%m", timeArray)
    resdict['日'] = time.strftime("%d", timeArray)
    resdict['号'] = time.strftime("%d", timeArray)
    return resdict

def 核心_时间_明天(d,g):
    resdict = {}
    today=datetime.date.today()
    delday=datetime.timedelta(days=1)
    yesterday=today+delday
    timeArray = time.strptime(yesterday.isoformat(), "%Y-%m-%d")
    resdict['年'] = time.strftime("%Y", timeArray)
    resdict['月'] = time.strftime("%m", timeArray)
    resdict['日'] = time.strftime("%d", timeArray)
    resdict['号'] = time.strftime("%d", timeArray)
    return resdict

def 核心_时间_后天(d,g):
    resdict = {}
    today=datetime.date.today()
    delday=datetime.timedelta(days=2)
    yesterday=today+delday
    timeArray = time.strptime(yesterday.isoformat(), "%Y-%m-%d")
    resdict['年'] = time.strftime("%Y", timeArray)
    resdict['月'] = time.strftime("%m", timeArray)
    resdict['日'] = time.strftime("%d", timeArray)
    resdict['号'] = time.strftime("%d", timeArray)
    return resdict
    
def 核心_时间_现在(d,g):
    resdict = {}
    timeStamp = int(time.time())
    timeArray = time.localtime(timeStamp)
    resdict['年'] = time.strftime("%Y", timeArray)
    resdict['月'] = time.strftime("%m", timeArray)
    resdict['日'] = time.strftime("%d", timeArray)
    resdict['号'] = time.strftime("%d", timeArray)
    resdict['时'] = time.strftime("%H", timeArray)
    resdict['分'] = time.strftime("%M", timeArray)
    resdict['秒'] = time.strftime("%S", timeArray)
    return resdict

def 核心_时间_年(d,g):
    resdict = {}
    year = d.get('年','')
    if(year==''):
        year=核心_时间_现在(d,g).get('年','')
    resdict['年'] = year+"年"
    return resdict

def 核心_时间_月(d,g):
    resdict = {}
    month = d.get('月','')
    if(month==''):
        month=核心_时间_现在(d,g).get('月','')
    resdict['月'] = month+"月"
    return resdict

def 核心_时间_日(d,g):
    resdict = {}
    day = d.get('日','')
    if(day==''):
        day=核心_时间_现在(d,g).get('日','')
    resdict['日'] = day+"号"
    return resdict

def 核心_时间_号(d,g):
    resdict = {}
    day = d.get('号','')
    if(day==''):
        day=核心_时间_现在(d,g).get('日','')
    resdict['号'] = day+"号"
    return resdict


def 核心_时间_时(d,g):
    resdict = {}
    hour = d.get('时','')
    if(hour==''):
        hour=核心_时间_现在(d,g).get('时','')
    resdict['时'] = hour+"点"
    return resdict

def 核心_时间_分(d,g):
    resdict = {}
    minute = d.get('分','')
    if(minute==''):
        minute=核心_时间_现在(d,g).get('分','')
    resdict['分'] = minute+"分"
    return resdict

def 核心_时间_秒(d,g):
    resdict = {}
    second = d.get('秒','')
    if(second==''):
        timeStamp = int(time.time())
        timeArray = time.localtime(timeStamp)
        second = time.strftime("%S", timeArray)
    resdict['秒'] = second+"秒"
    return resdict

def 核心_时间_星期(d,g):
    resdict = {}
    week = d.get('星期','')
    if(week==''):
        timeStamp = int(time.time())
        timeArray = time.localtime(timeStamp)
        year = d.get('年',time.strftime("%Y", timeArray)).replace('年','')
        month = d.get('月',time.strftime("%m", timeArray)).replace('月','')
        day = d.get('日',d.get('号',time.strftime("%d", timeArray)).replace('号','')).replace('号','')
        timeArray = time.strptime(year+"-"+month+"-"+day, "%Y-%m-%d")
        week = weekdic.get(time.strftime("%a", timeArray))
    resdict['星期'] = week
    return resdict

def 核心_时间_时候(d,g):
    resdict = {}
    hour = d.get('时','').replace('点','')
    minute = d.get('分','').replace('分','')
    if(hour=='' or minute==''):
        timeStamp = int(time.time())
        timeArray = time.localtime(timeStamp)
        hour = time.strftime("%H", timeArray)
        minute = time.strftime("%M", timeArray)
    elif(hour==''):
        return resdict
    tag = ""
    ihour = int(hour)
    if(ihour>=0 and ihour<6):
        tag = "凌晨"
    elif(ihour>=6 and ihour<9):
        tag = "早上"
    elif(ihour>=9 and ihour<11):
        tag = "上午" 
    elif(ihour>=11 and ihour<13):
        tag = "中午"  
    elif(ihour>=13 and ihour<17):
        tag = "下午" 
    elif(ihour>=17 and ihour<19):
        tag = "傍晚" 
    elif(ihour>=19 and ihour<24):
        tag = "晚上"
    if(minute==''):
        resdict['时候'] = tag
    else:
        resdict['时候'] = tag + "%s点%s分"%(hour,minute)
    return resdict

def 核心_时间_点(d,g):
    resdict = {}
    hour = d.get('时','')
    if(hour==''):
        hour = 核心_时间_现在(d,g).get('时','')
    resdict['点'] = hour+"点"
    resdict['点钟'] = hour+"点"
    return resdict

def 核心_时间_时区(d,g):
    resdict = {}
    timezone = d.get('时区','')
    if(timezone==''):
        timeStamp = int(time.time())
        timeArray = time.localtime(timeStamp)
        timezone = time.strftime("%z", timeArray)
    resdict['时区'] = timezone
    return resdict

def 核心_时间_日期(d,g):
    resdict = {}
    year = d.get('年','')
    month = d.get('月','')
    day = d.get('日','')
    if(day==''):
        day = d.get('日','')
    if(year=='' and month=='' and day==''):
        timeStamp = int(time.time())
        timeArray = time.localtime(timeStamp)
        year = d.get('年',time.strftime("%Y", timeArray)).replace('年','')
        month = d.get('月',time.strftime("%m", timeArray)).replace('月','')
        day = d.get('日',d.get('号',time.strftime("%d", timeArray)).replace('号','')).replace('号','')
    resdict['日期'] = year+"年"+month+"月"+day+"日"
    return resdict

def 核心_时间_时间(d,g):
    resdict = {}
    hour = d.get('时','')
    minute = d.get('分','')
    second = d.get('秒','')
    if(hour=='' and minute=='' and second==''):
        timeStamp = int(time.time())
        timeArray = time.localtime(timeStamp)
        resdict['时间'] = time.strftime("%X", timeArray)
    else:
        resdict['时间'] = hour+":"+minute+":"+second
    return resdict
