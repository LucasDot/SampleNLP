#运行文件 人工智能.py
#作者 幽灵代码
#QQ 29672366
#邮箱 29672366@qq.com
#日期 2016.3.21

import os 
import fnmatch
import sys

rootpath = ''
#rootpath = '/storage/emulated/0/com.hipipal.qpyplus/'

sys.path.append(rootpath + '核心')
#sys.path.insert(0,rootpath +'core')

import sqlite3 as db

#加载数据库
cx=db.connect(rootpath+"data.db")
cu=cx.cursor()

g = {}

#自然语言解释
def nlpexp(d):
    lstdict = []
    sentense = d["待处理语句"]
    if(sentense==""):
        return lstdict
    sql = "select phrase,modulename,typedef from executemap where '"+sentense+"' like phrase||'%' order by length(phrase) desc "
    #print (sql)
    cu.execute(sql)
    for row in cu.fetchall():
        resdict={}
        if(row[2]=="属性"):
            if "浮点数" in d:
                if row[0] in d:
                    if d[row[0]].replace(row[0],'')==d["浮点数"]:
                        d["消息"] = "是的"
                    else:
                        resdict = eval(row[1] +"_分发器",g,g)(row[0],d,g)
                        d["消息"] = "不,是"+resdict[row[0]]
                else:
                    d[row[0]] = d["浮点数"]
                del d["浮点数"]
            elif "意识" in d:
                if(d["意识"] == "疑问"):
                    t = {"告知属性":row[0]}
                    wordexp("任务队列",t)
                    del d["意识"]
            else:
                d["未处理属性"] = row[0]
        resdict = eval(row[1] +"_分发器",g,g)(row[0],d,g)
        for k in resdict:
            if k in d:
                del d[k]
        resdict = dict(resdict, **d)
        resdict["待处理语句"] = sentense.split(row[0],1)[1]
        lstdict.append(resdict)
    #print(lstdict)
    return lstdict

#词汇解释
def wordexp(phrase,d):
    lstdict = []
    sql = "select modulename,typedef from executemap where phrase='%s'"%(phrase)
    #print (sql)
    cu.execute(sql)
    for row in cu.fetchall():
        resdict={}
        resdict = eval(row[0] +"_分发器",g,g)(phrase,d,g)
        lstdict.append(resdict)
    return lstdict
    
#模块注册
def register(modelname,version):
    #版本检测 查找数据库中是否已注册此版本模块 若已注册则激活
    sql = "select modelname,vername from register where modelname = '%s'"%(modelname)
    cu.execute(sql)
    #print(sql)
    result =cu.fetchone()
    if(result==None):
        print("检测到新的模块")
        des = eval(modelname +"_描述",g,g)()
        print("   模块名:%s"%modelname)
        print("   版本号:%s"%version)
        print("   用途:%s"%des)
        sql = "insert or replace into register (modelname,vername,useable)\
        values ('%s','%s',%d)"%(modelname,version,True)
        cu.execute(sql)
        funcdict = eval(modelname +"_分类映射表",g,g)()
        for key in funcdict:
            sql = "insert or replace into executemap(phrase,modulename,typedef)\
            values ('%s','%s','%s')"%(key,modelname,"分类")
            cu.execute(sql)
        cx.commit()
        funcdict = eval(modelname +"_对象映射表",g,g)()
        for key in funcdict:
            sql = "insert or replace into executemap(phrase,modulename,typedef)\
            values ('%s','%s','%s')"%(key,modelname,"对象")
            cu.execute(sql)
        cx.commit()
        funcdict = eval(modelname +"_属性映射表",g,g)()
        for key in funcdict:
            sql = "insert or replace into executemap(phrase,modulename,typedef)\
            values ('%s','%s','%s')"%(key,modelname,"属性")
            cu.execute(sql)
        cx.commit()
        funcdict = eval(modelname +"_执行映射表",g,g)()
        for key in funcdict:
            sql = "insert or replace into executemap(phrase,modulename,typedef)\
            values ('%s','%s','%s')"%(key,modelname,"执行")
            cu.execute(sql)
        cx.commit()
    else:
        if(result[1]==version):
            #激活模块
            sql = "update register set useable = 1 \
            where modelname = '%s' and vername = '%s'\
            "%(modelname,version)
            cu.execute(sql)
            cx.commit()
            print("模块:%s 已激活"%modelname)
        else:
            #模块更新
            return
    
#模块检测
def loadAndCheck(modelname):
    #print("loadAndCheck:"+modelname)
    impstr = "from " + modelname + " import " + modelname + "_版本"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_描述"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_分类映射表"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_对象映射表"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_属性映射表"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_执行映射表"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_初始化"
    exec(impstr,g,g)
    impstr = "from " + modelname + " import " + modelname + "_分发器"
    exec(impstr,g,g)
    #print(impstr)
    ver = eval(modelname +"_版本",g,g)()
    register(modelname,ver)
    ver = eval(modelname +"_初始化",g,g)(wordexp,nlpexp)
    
#文件搜索函数
def iterFindFiles(path, fnexp): 
    for root, dirs, files in os.walk(path): 
        for filename in fnmatch.filter(files, fnexp): 
            yield filename

#重置所有模块
def reset():
    sql = "update register set useable = 0"
    cu.execute(sql)
    cx.commit()

reset()

#搜索核心模块 
for filename in iterFindFiles(rootpath+"核心", "核心_*.py"):
    loadAndCheck(filename[:-3])


while(True):
    userinput = input("输入语句：")
    d = {"待处理语句":userinput}
    eval("核心_感知_分发器",g,g)("终端输入",d,g)
    



